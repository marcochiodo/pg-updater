<?php

namespace PgUpdater;

class Helper
{

	private $Resource;

	public function __construct( $Resource )
	{
		$this->Resource = $Resource;
	}

	public function launchQueires( $Queries )
	{
		
		$ok = true;
		
		foreach( $Queries as $key => $value )
		{

			echo "$key -> ";
			
			$result = pg_query( $this->Resource , $value );

			if( $result ){
				echo "OK".PHP_EOL;
			} else {
				echo "FAIL".PHP_EOL;
				$ok = false;
				break;
			}
			
		}
		
		return $ok;
		
	}
}

?>