<?php

namespace PgUpdater;

class Client
{
	
	private $config;
	private $Resource;

	public function __construct( array $config )
	{
		$this->config = $config;
	}

	public function getResource()
	{
		if( empty($this->Resource) )
			$this->Resource = pg_connect($this->config['connection']);

		return $this->Resource;
	}

	public function run()
	{

		echo "\n --- Update DB action --- \n\n";


		$Result = pg_query( $this->getResource() , "SELECT description FROM pg_description WHERE  objoid = 'system.db_update_history'::regclass;");

		if( $Result != false && count($Result) == 1 ){


			$Row = pg_fetch_assoc( $Result );
			$description = $Row['description'];
			$queries_dir = $this->config['queries_dir'];

			$listQueries = scandir( $queries_dir , SCANDIR_SORT_ASCENDING );

			$ok = true;

			echo 'STARTING TRANSACTION'.PHP_EOL;

			pg_query("BEGIN") or die("Could not start transaction\n");

			foreach ( $listQueries as $inputFile ){

				if( ($inputFile[0] != '.' ) && ($inputFile > "$description.php") ){

					echo "\n --- Update -->> #". pathinfo($inputFile,PATHINFO_FILENAME ) ."# --- \n\n";

					include $queries_dir.'/'.$inputFile;

					$ClassName = 'Executables\\'.pathinfo($inputFile,PATHINFO_FILENAME );
					$Class = new $ClassName( $this->getResource() );

					if( ! $Class->run() ){

						$ok = false;
						break;
					}

					echo "\nUpdate flag: ".pathinfo($inputFile,PATHINFO_FILENAME )." ... ";

					$Result = pg_query( $this->getResource() , "COMMENT ON TABLE system.db_update_history IS '".pathinfo($inputFile,PATHINFO_FILENAME ) ."'" );

					if( ! $Result ){

						$ok = false;
						break;
					}

					echo "\n\n --- End -->> #". pathinfo($inputFile,PATHINFO_FILENAME ) ."# --- \n\n";

					unset($Class);
				}
			}

			if( $ok ) {

				echo "COMMITING TRANSACTION".PHP_EOL;
				pg_query("COMMIT") or die("Transaction commit failed\n");
			} else {
				echo "ROLLING BACK TRANSACTION".PHP_EOL;
				pg_query("ROLLBACK") or die("Transaction rollback failed\n");
			}



		} else {

			echo 'Something not work here...';
		}



		echo "\n\n --- ----------------- --- \n";

	}


}

?>
